﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TS.CreditCardValidator.Core.Enums
{
    public enum CreditCardType
    {
        Amex,
        Discover,
        MasterCard,
        Visa,
        Undefined
    }
}
