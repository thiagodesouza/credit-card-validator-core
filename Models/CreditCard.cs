﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TS.CreditCardValidator.Core.Enums;
using TS.CreditCardValidator.Core.Utils;

namespace TS.CreditCardValidator.Core.Models
{
    [Serializable]
    public class CreditCard
    {
        public CreditCard(string Number)
        {
            this.Number = (Number != null) ? Number.Replace(" ", string.Empty) : string.Empty;
        }

        public string Number { get; private set; }

        public string Type
        {
            get => new CreditCardTypeVerification(this.Number).Result.ToString(); 
        }

        public bool IsValid
        {
            get => new CreditCardNumberValidation(this.Number).Result;
        }
    }
}
